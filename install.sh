#!/bin/bash

SYSCTL=$(which systemctl)
INIT=$(which init)


if [[ $0 != './install.sh' ]];then
    cd $(dirname $0)
    RET=1
fi
    
EXEC_PATH="/usr/local/bin"
BASH=$(which bash)

if [[ -x $SYSCTL && $($SYSCTL) =~ -\.mount ]]; then SYSTEM="systemd";
elif [[ $($INIT --version) =~ upstart ]]; then SYSTEM="upstart";
elif [[ -f /etc/init.d/cron && ! -h /etc/init.d/cron ]]; then SYSTEM="sysv-init";
else echo "unknown system init process";exit 1
fi


# Protects monitor.cfg files from
# being replaced, but always replaces monitor (if there are updates)...
# and monitor.cron


DIR="/etc/monitor"
test -d $DIR || mkdir $DIR 
test -f $DIR/monitor.cfg  || cp ./monitor.cfg  $DIR
test -f $DIR/number.cfg   || cp ./number.cfg   $DIR
test -f $DIR/services.cfg || cp ./services.cfg $DIR
test -f $DIR/contact.cfg  || cp ./contact.cfg  $DIR
chown root:root $DIR/monitor.cfg
chown root:root $DIR/number.cfg
chown root:root $DIR/services.cfg
chown root:root $DIR/contact.cfg


if [[ ! -f $EXEC_PATH/monitor ]]; then
   cp ./monitor $EXEC_PATH
else 
   diff ./monitor $EXEC_PATH/monitor >/dev/null || \
     cp ./monitor $EXEC_PATH 
fi

chown root:root $EXEC_PATH/monitor
chmod +x $EXEC_PATH/monitor

test -d /var/lib/monitor || mkdir /var/lib/monitor

if [[ $SYSTEM == "sysv-init" ]]; then 
    cp sysv/monitor.cron /etc/cron.d/monitor
    chown root:root /etc/cron.d/monitor
    sed -i "s?EXEC_PATH?$EXEC_PATH?" /etc/cron.d/monitor
fi

if [[ $SYSTEM == "systemd" ]]; then 
    cp systemd/monitor.timer /lib/systemd/system/
    cp systemd/monitor.service /etc/systemd/system/
    chown root:root /lib/systemd/system/monitor.timer
    chown root:root /etc/systemd/system/monitor.service
    sed -i "s?EXEC_PATH?$EXEC_PATH?"  /etc/systemd/system/monitor.service
    sed -i "s?BASH?$BASH?" /etc/systemd/system/monitor.service
    systemctl enable monitor.service
    systemctl enable --now monitor.timer
    systemctl daemon-reload
    systemctl status monitor.service
    sleep 10
    systemctl status monitor.timer
fi

if [[ $RET == 1 ]]; then
   cd ->/dev/null
fi
