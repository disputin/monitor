#!/usr/bin/env bash

#########################################################################
# This is monitor a very simple shell script that can be used to        # 
# monitor a set of servers or hosts.  It attempts to notify in a        #
# manner that isn't annoying it has a config directory with several     #
# files, and a state directory for current and past processing. It has  #
# no webpage or status or anything else, it just sends notifications.   #
#########################################################################

#########################################################################
#                                                                       #
# Copyright (C) 2020  Sean Whitney                                      #
#                                                                       #
# This program is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with this program. If not, see <https://www.gnu.org/licenses/>. #
#                                                                       #
#########################################################################

#########################################################################
# set up variables, file locations, some default values and read in     #
# some values from the configuration files                              #
#########################################################################

CDIR="/etc/monitor"
# define the default contact(s)
source $CDIR/contact.cfg
MONDIR="/var/lib/monitor"
CONFIG="$CDIR/monitor.cfg"
DC=0
POS=0
URL="http://textfelt.com/text"

# This contains the periods when you should be informed 
# that a problem is repeating.
# This way if something is failing you won't be bothered 
# every 5 minutes or however long you set it to check

DEFAULT_INFORM=( 300 900 1800 3600 7200 21600 43200 64800 86400 )
INFORM=()

# Hopefully you don't have to change much beyond this point

#############################################################
# check to make sure all necessary software is install or   #
# exit, if you do exit here read the README.md file for     #
# instructions                                              #
#############################################################

NC=$(which netcat)
TS=$($(which date) +%s)
UN=$(which uname)
LOG="$(which logger) -i -t monitor " 
CURL=$(which curl)
HST=$(which host)
PING=$(which ping)
MAIL=$(which mail)
test -f $MAIL || MAIL=$(which mailx)
test "x$TS"   = x && echo $0 requires date package           && exit 1
test "x$NC"   = x && echo $0 requires netcat package         && exit 1
test "x$UN"   = x && echo $0 requires uname package          && exit 1
test "x$LOG"  = x && echo $0 requires logger package         && exit 1
test "x$HST"  = x && echo $0 requires host package           && exit 1
test "x$CURL" = x && echo $0 requires curl package           && exit 1
test "x$PING" = x && echo $0 requires ping package           && exit 1
test "x$MAIL" = x && echo $0 requires mail or mailx package  && exit 1

##########################################################################
# Some services behave better when presented with a text string          #
# Found this https://bitbucket.org/tildeslash/monit/commits/79cc6015c2a9 #
##########################################################################

MYSQL_SEND='\x22\x00\x00\x01\x01\x82\x00\x00\x00\x00\x00\x01\x08\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
SSH_SEND="SSH-2.0-OpenSSH $($UN)"

##########################################################################
# some administrative functions                                          #
##########################################################################

function log() {
    $LOG "$@"
}

# http://www.linuxjournal.com/content/validating-ip-address-bash-script

function valid_ip() {
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

function checkhost() {
    if ! valid_ip $HOST; then
        if ! $HST $HOST>/dev/null 2>&1; then
            echo $HOST is not a valid hostname/IP
            echo Please provide a valid IP address or hostname.
            echo
            $0 -h
            exit 1
        fi
    fi
}

function inform_process() {
    if [[ -f $MONDIR/inform ]]; then
	source $MONDIR/inform	
    else
	INFORM=( "${DEFAULT_INFORM[@]}" )
    fi

}

# The timestamp in the file isn't used to indicate when the 
# alarm was initiated, it is used to trigger an SMS message.

function notify() {
    if [[ -f $MONDIR/notified.$HOST.$SERVICE ]];then
        read -r OTS STEP < $MONDIR/notified.$HOST.$SERVICE
        if (( $TS - $OTS >= ${INFORM[$STEP]} )); then
# Deal with case of the array at the end value
            if (( $STEP + 1 < ${#INFORM[@]} )); then
                let STEP++
	    fi

# If the difference between the old timestamp and the current 
# timestamp is greater than the last value in the array, add the 
# last value of the array to the old timestamp 

            if (( $TS - $OTS >=  ${INFORM[ (( ${#INFORM[@]} - 1 )) ]} )); then
                TMP=$(( $OTS + ${INFORM[ (( ${#INFORM[@]} - 1 )) ]} ))	
                log "notified.$HOST.$SERVICE timestamp updated from $OTS to $TMP"
	         OTS=$TMP
            fi 
	    POS=$STEP
            TS=$OTS
	    return 0
        else
	    POS=$STEP
	    TS=$OTS
	    return 1
        fi
    else
        return 0
    fi	
}

# Attempt to purge number of non numeric characters
# however if number is for email to sms gateway skip

function clean_recipient() {

# https://stackoverflow.com/questions/2138701/checking-correctness-of-an-email-address-with-a-regular-expression-in-bash

    regex="^[a-z0-9!#\$%&'*+/=?^_\`{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_\`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?\$"
    for num in ${CONTACT[@]}; do
        unset nnum
       	if [[ ${num} =~ $regex ]]; then
           continue 
        elif [[ ${num} != *"@"* ]]; then
            ((MAX=${#num[@]} - 1 ))
            for ((i = 0; i <=$MAX ; i++)); do
	        nnum[$i]=$(echo ${num[$i]}|tr -dc [:digit:])
	    done

# find and replace the pattern with the string, parameter/pattern/string 

            CONTACT=( ${CONTACT[@]/$num/$nnum} )	
        else
            log "Unable to determine address type of $num, exiting"
            exit 1
        fi
    done
}


##############################################################################
# This section is responsible for sending notifications to CONTACTS           #
##############################################################################

function send_msg() {
    test "$debug" == 'true' && echo "Sent message $2 to $1"

# this isn't the best check of a address but it's what I got right
# now. I don't have a sms gateway to test anymore

    if [[ ${1} != *"@"* ]]; then

# This line would need to be customized for texting to a sms gateway
# This worked with textbelt.com for a while

        $CURL -X POST $URL -d number=$1 -d "message=$2">/dev/null 2>&1
        log "sent $1, message $2"
    elif [[ ${1} == *"@"*  ]]; then
        echo $2 |$MAIL ${1} -s "monitor"
        log "sent $1, message $2"
    else
        log "unable to find method of reporting."
        log "did not sent $1, message $2"
    fi
}

# This optional function will requires two consecutive outage
# detections prior to sending a notification.  This stops instances
# where hosts are detected down and recover in the next check

function outage_detected() {
    SERVICE=$1
    if [[ -f $MONDIR/notified.$HOST.$SERVICE ]]; then
        notify_outage $SERVICE
    elif [[ -f $MONDIR/detected.$HOST.$SERVICE ]] && \
         [[ ! -f $MONDIR/notified.$HOST.$SERVICE ]]; then
        rm $MONDIR/detected.$HOST.$SERVICE
        notify_outage $SERVICE
    else
        echo "$TS $POS" >  $MONDIR/detected.$HOST.$SERVICE
        log "created $MONDIR/detected.$HOST.$SERVICE"	
    fi
}

function notify_outage() {
    SERVICE=$1
    test "$debug" == 'true' && echo $SERVICE failed for $HOST
    if notify; then
        for CNTCT in "${CONTACT[@]}"; do
            send_msg $CNTCT "contacting $HOST via $SERVICE failed, please investigate"
        done
    fi

# If the outage_detected function is being used, keep
# the initial timestamp....

    if [[ -f $MONDIR/detected.$HOST.$SERVICE ]]; then
        mv $MONDIR/detected.$HOST.$SERVICE \
           $MONDIR/notified.$HOST.$SERVICE
    else
        echo "$TS $POS" >  $MONDIR/notified.$HOST.$SERVICE
    fi
}

function notify_recovery() {
    SERVICE=$1
    test "$debug" == 'true' && echo $SERVICE recovered for $HOST
    for CNTCT in "${CONTACT[@]}"; do
        send_msg $CNTCT "contacting $HOST via $SERVICE has recovered"
    done
}

function ping() {
    test "$p" == 'false' && return
    if ! $PING -c 1 $HOST>/dev/null; then
# if ping fails, check once again, to avoid false positives
# if second ping fails then proceed normally
        if [[ $DC -eq 0 ]]; then
            DC=1
            ping
            DC=0
        elif [[ "$r" == "true" && $DC -ge 1 ]];then
            outage_detected 'ping' 
        else
            notify_outage 'ping' 
        fi

# If there are service detections/notifications present
# They are irrelevant until the host is back up

        if ls $MONDIR/*.$HOST.* 2>/dev/null|grep -v ping; then
            for file in $(ls $MONDIR/*.$HOST.*|grep -v ping); do
                log "because $HOST is down, removed $file"
                rm $file
            done
        fi

# If you get to here the host is down so exit

	exit 0
    else
        if [[ -f $MONDIR/notified.$HOST.ping ]]; then
            rm $MONDIR/notified.$HOST.ping
            notify_recovery "ping"
        fi
	test -f $MONDIR/detected.$HOST.ping && \
             rm $MONDIR/detected.$HOST.ping
    fi
}

###########################################################################
# This section contains the different service checks.  It can handle TCP  #
# UDP and TCP services requiring special strings, such as SSH or MySQL    #
###########################################################################

function netcat_tcp_send() {
    PORT=$1
    SERVICE=$2
    STRING=$3
    if ! echo -ne $STRING|$NC -z $HOST $PORT>/dev/null 2>&1; then
        if [[ "$r" == "true" && $DC -eq 0 ]]; then
            DC=1
            netcat_tcp_send $PORT $SERVICE $STRING
            DC=0
        elif [[ "$r" == "true" && $DC -ge 1 ]]; then
            outage_detected $SERVICE
	else
	    notify_outage $SERVICE
	fi
    else
        if [[ -f $MONDIR/notified.$HOST.$SERVICE ]]; then
	    rm $MONDIR/notified.$HOST.$SERVICE && \
            notify_recovery $SERVICE
        fi
	test -f $MONDIR/detected.$HOST.$SERVICE && \
             rm $MONDIR/detected.$HOST.$SERVICE
    fi
}

function netcat_tcp_chk() {
    PORT=$1
    SERVICE=$2
    if ! $NC -z${3} $HOST $PORT>/dev/null 2>&1; then
        if [[ "$r" == "true" && DC -eq 0 ]]; then
            DC=1
            netcat_tcp_chk $PORT $SERVICE
            DC=0
        elif [[ "$r" == "true" && $DC -ge 1 ]]; then
            outage_detected $SERVICE
	else
	    notify_outage $SERVICE
	fi
    else
        if [[ -f $MONDIR/notified.$HOST.$SERVICE ]];then
            rm $MONDIR/notified.$HOST.$SERVICE && \
            notify_recovery $SERVICE
        fi
	test -f $MONDIR/detected.$HOST.$SERVICE && \
             rm $MONDIR/detected.$HOST.$SERVICE
    fi
}

function netcat_udp_chk() {
    PORT=$1
    SERVICE=$2
    netcat_tcp_chk $PORT $SERVICE uv
}

##############################################################
# This section is for processing the config file             #
##############################################################

function config_process() {

# ensure that there is no left over inform file
# from a previous call, this should never have
# to be used

    test -f $MONDIR/inform && rm $MONDIR/inform

# process the config file, line by line, processing
# lines starting with CONTACT or INFORM differently

    while read line;do
        [[ -z "$line" ]] && continue
	[[ $line =~ ^#.*$ ]] && continue
	if [[ "$line" =~ ^(CONTACT).*$ ]]; then 
	    eval $line
	    clean_recipient
            continue
        fi
	if [[ "$line" =~ ^(INFORM).*$ ]]; then 
	    eval $line
            TMP=( ${line:-$INFORM} )
	    echo ${TMP[@]} > $MONDIR/inform
            continue
        fi
# -z is a flag only used by the program to ensure there isn't a 
# nasty loop if someone accidently put a -c argument in the config
# file resulting in an infinate loop
        line=$(echo $line|sed 's/^\-/-z/')
	$0 $line ${CONTACT[*]}
    done < $CONFIG 

# this is a bit scary, if it doesn't get deleted somehow it will 
# hang around until the next time it is run calling a config file
# but you can't remove it elsewhere because it will screw up the
# the $0 call previously

    test -f $MONDIR/inform && rm $MONDIR/inform
    exit 0
}

#################################################################
# This is the main section                                      #
#################################################################

while getopts ":ac:CdDfhijmNno:prsStTvz?" opt; do
    case $opt in
        a) a='true' ;;
        c) c='true';PCONFIG=${OPTARG} ;;
        C) C='true' ;;
        d) d='true' ;;
        D) D='true' ;;
        f) f='true' ;;
        i) i='true' ;;
        j) j='true' ;;
        m) m='true' ;;
        N) N='true' ;;
        n) n='true' ;;
        o) o='true';ESERVICE=${OPTARG} ;;
        p) p='false' ;;
        r) r='true' ;;
        s) s='true' ;;
        S) S='true' ;;
        t) t='true' ;;
        T) T='true' ;;
        v) debug='true' ;;
        z) z='true' ;;
        \?|h)
            echo "monitor takes the following arguments:"
            echo " -a test asterisk"
            echo " -C test cups"
            echo " -d test dhcp/bootps"
            echo " -D test dns/domain"
            echo " -f test ftp"
            echo " -i test http"
            echo " -j test https"
            echo " -m test mysql"
            echo " -N test ntp"
            echo " -n test netatalk"
            echo " -s test ssh"
            echo " -S test smtp"
            echo " -t test telnet"
            echo " -T test tftp"
            echo " -c process a monitor config file, if no argument"
            echo "    is passed the default is used /etc/monitor.cfg"
            echo "    an argument lets you run multiple instance of" 
            echo "    monitor at the same time with different groups"
            echo "    of hosts"
            echo "    and a list of phone numbers if included"
            echo " -o define your own service. Define a service in"
            echo "    /etc/monitor/services.cfg and you can extend"
            echo "    monitor with your own services."
            echo "    -sj -omondo hostname can be used to check a"
            echo "    host ssh, https and mondo services"
            echo "    multiple services can be passed with -o mondo,jira,git "
            echo "    determining the method of checks is left as an exercise"
            echo "    for the user"
            echo " -r suppress immediate notification.  Once the check fails,"
            echo "    it will immediately recheck. If this fails a flag is set"
            echo "    During the next period another check will be conducted,"
            echo "    if this has failed a notification will be sent. This"
            echo "    can be used for host with limited connectivity issues"
            echo " -v verbose or debug.  Generally this script should be"
            echo "    run out of cron and no command line feedback is presented"
            echo " -p suppress ping test, prior to testing other connection"
            echo
            echo "These arguments need to be followed by a hostname."
            echo "except for -c which should be followed by and alternative"
            echo "configuration file path such as /etc/monitor/jirahosts.cfg"
            echo "Additionally a phone number(s) can be provided on the command"
            echo "for notification. These will replace any numbers hardcoded"
            echo "in the script.  Only providing a hostname will ping a host."
            exit 1 ;;
        :) ;;
    esac
done


shift "$((OPTIND - 1))"
test -d $MONDIR || mkdir -p $MONDIR
test -w $MONDIR || chown $UID:$GROUPS $MONDIR

if [[ ( "$z" != 'true' && "$c" == 'true' ) || $# -eq 0 ]]; then
   CONFIG=${PCONFIG:-$CONFIG}
   if [[ ! -e $CONFIG ]]; then
      echo "unable to find $CONFIG" 
      exit 1
   else
      config_process
      exit 0
   fi
else 
    HOST=$1
    shift
    CONTACT=( ${@:-$CONTACT} )
    if [[ -z ${CONTACT} ]]; then
        echo No default number found in $CDIR/contact.cfg
        echo Please set the default number
        exit 1
    fi
    clean_recipient

    if [[ -z $HOST ]]; then
    	 $0 -h
         exit 1
    fi

    inform_process
    checkhost
    ping
    test "$a" == 'true' && netcat_tcp_chk  5038  asterisk
    test "$C" == 'true' && netcat_tcp_chk  631   cups 
    test "$d" == 'true' && netcat_udp_chk  67    bootps
    test "$D" == 'true' && netcat_udp_chk  53    domain
    test "$f" == 'true' && netcat_tcp_chk  21    ftp 
    test "$i" == 'true' && netcat_tcp_chk  80    http
    test "$j" == 'true' && netcat_tcp_chk  443   https
    test "$m" == 'true' && netcat_tcp_send 3306  mysql $MYSQL_SEND 
    test "$N" == 'true' && netcat_udp_chk  123   ntp 
    test "$n" == 'true' && netcat_tcp_chk  548   netatalk 
    test "$o" == 'true' && source /etc/monitor/services.cfg $ESERVICE
# $p handled at the beginning of ping function
# $r handled elsewhere
    test "$s" == 'true' && netcat_tcp_send 22    ssh $SSH_SEND 
    test "$S" == 'true' && netcat_tcp_chk  25    smtp 
    test "$t" == 'true' && netcat_tcp_chk  23    telnet 
    test "$T" == 'true' && netcat_udp_chk  69    tftp 
fi
exit 0
