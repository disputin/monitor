# monitor
This is a simple shell script that can be used to monitor hosts and services, and send a sms or an email or a sms/email notification when a outage is detected. monitor conducts a simple ping test on a host and if this is successful, it will proceed to check on the service ports to make sure they are responding appropriately.

```
monitor takes the following arguments:
 -a test asterisk
 -C test cups
 -d test dhcp/bootps
 -D test dns/domain
 -f test ftp
 -i test http
 -j test https
 -m test mysql
 -n test netatalk
 -s test ssh
 -S test smtp
 -t test telnet
 -T test tftp
 -c process a monitor config file, if no argument
    is passed the default is used /etc/monitor.cfg
    an argument lets you run multiple instance of
    monitor at the same time with different groups
    of hosts
    and a list of phone contacts if included
 -o define your own service. Define a service in
    /etc/monitor/services.cfg and you can extend
    monitor with your own services.
    -sj -omondo hostname can be used to check a
    host ssh, https and mondo services
    multiple services can be passed with -o mondo,jira,git 
    determining the method of checks is left as an exercise
    for the user
 -r suppress immediate notification.  Once the check fails,
    it will immediately recheck. If this fails a flag is set
    During the next period another check will be conducted,
    if this has failed a notification will be sent. This
    can be used for host with limited connectivity issues
 -v verbose or debug.  Generally this script should be
    run out of cron and no command line feedback is presented
 -p suppress ping test, prior to testing other connection

These arguments need to be followed by a hostname.
except for -c which should be followed by and alternative
configuration file path such as /etc/monitor/jirahosts.cfg
Additionally a phone number(s) can be provided on the command
for notification. These will replace any numbers hardcoded
in the script.  Only providing a hostname will ping a host.
```

##### Examples  

    monitor
Process the /etc/monitor/monitor.cfg file and send any notifications to the destinations in the contact.cfg or in each section of the monitor.cfg file, also use INFORM settings if they are included in the monitor.cfg file.  

    monitor -c /etc/monitor/jirahosts.cnf
Process the /etc/monitor/jirahosts.cnf file and send any notifications to the destinations in the contact.cfg or in each section of the jirahosts.cfg file, also use INFORM settings if they are included in the monitor.cfg file. This should probably be added to a cron file

    monitor -s webserver
Check the webserver host and the ssh process on the host and send any notifications to the default number.  

     monitor -smi databaseserver 5555555555
Check the database mysql server, ssh and http processes and send any notifications to 5555555555.  

    monitor firewall
Check the firewall host with a simple ping test and send any notification to the default number  

    monitor -ps firewall
Check the firewall ssh service, without a ping test and send any notifications to the default number  
 
These additional settings can be configurated in the monitor script  
* CONFIG - the location of the monitor.cfg file, Update this to a new location or filename  
* INFORM - this is an array of time (in seconds) which will repeat a notification.  This keeps monitor from sending the same sms message every five minutes. The default array will renotify at 5 minutes, 15 minutes, 30 minutes, 1 hour, 2 hours, 6 hours, 12 hours, 18 hours, and every 24 hours. The last value will be repeated until the issue is resolved.  

##### Required software

Monitor requires the following commands which should already be installed in a standard distribution ping, date, uname, netcat, logger, host, curl, and sed.

If you find any of these missing, for debian/ubuntu

   `sudo apt-get install iputils-ping coreutils netcat-openbsd bsdutils bind9-host curl sed `

for centos/fedora/redhat

   `sudo yum install iputils coreutils nc util-linux bind-utils curl sed which`

It is up to the user to determine how to install these packages for other Unix/Linux distributions.

##### Installation

This simple script will install monitor in the default location of /usr/local/bin.  If you want to install somewhere else, you will need to edit install.sh first.  It will never replace an existing /etc/monitor.cfg file, however it will always replace the cron and monitor executable files, this allows it to be used for monitor upgrades as well.

`sudo ./install.sh `

There are two files that needs to be set manually at installation and the first is /etc/monitor/contact.cfg.  This is the default notification contact(s) for the applicatio.  It is one line with the notification number(s) or email to sms address(es), no other values or settings.  monitor will refuse to run unless this is set.

    CONTACT = ( 5551239876 dutypager@company.net 5551238765@tmomail.net ) 

Once that is set, you can run monitor manually from the commandline for testing. 
The second file that needs to be configured it /etc/monitor/monitor.cfg. The file monitor.cfg.samples shows examples but will need to be configured with meaningful host information and services information.

Within the monitor.cfg file you can set additional CONTACT or INFORM stanzas.  Once a new CONTACT or INFORM stanza is set it will be used for all following hosts, until a new CONTACT or INFORM stanza is detected or the end of the file is reached.  This allows different groups to be informed of different hosts within monitor or for escalaction of notification.
